from flask import Flask, render_template, request, redirect, url_for, flash
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager, UserMixin, current_user, login_user, logout_user
# from libs.user import User
from oauth import OAuthSignIn


app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {"DB": "my_tumble_log"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"

app.config['OAUTH_CREDENTIALS'] = {
    'twitter': {
        'id': '<put_your_id',
        'secret': '<put_your_secret'
    }
}

db = MongoEngine(app)
lm = LoginManager(app)
lm.login_view = 'index'



class User(db.Document, UserMixin):
    social_id = db.StringField(default=True)
    nickname = db.StringField(default=False)
    active = db.BooleanField(default=True)
    id = db.StringField(default=True)

    def get_by_social_id(self, social_id):
        dbUser = User.objects.get(social_id=social_id)
        if dbUser:
            self.social_id = dbUser.social_id
            self.nickname = dbUser.nickname
            self.id = dbUser.id

            return self
        else:
            return None


@lm.user_loader
def load_user(id):
    return User.objects.with_id(id)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    if not current_user.is_anonymous():
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@app.route('/callback/<provider>')
def oauth_callback(provider):
    if not current_user.is_anonymous():
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    social_id, username = oauth.callback()
    if social_id is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))

    user = User()
    try:
        user.get_by_social_id(social_id)
    except:
        user=None

    if not user:
        user = User(social_id=social_id, nickname=username, active=True)
        user.save()

    login_user(user, True)
    return redirect(url_for('index'))
